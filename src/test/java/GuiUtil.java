import javax.swing.*;
import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.net.URL;

public class GuiUtil {
   JWindow window;

  public void show(int step, int seconds) throws MalformedURLException, InterruptedException {
    window = new JWindow();
    window.setAlwaysOnTop(true);
    window.setSize(Toolkit.getDefaultToolkit().getScreenSize());
    window.getContentPane().add(
        new JLabel("", new ImageIcon(getClass().getClassLoader().getResource(step + ".png")), SwingConstants.CENTER));

   // window.setBounds(500, 150, 800, 600);
    window.setVisible(true);

    Thread.sleep(seconds * 1000);
    window.setVisible(false);

  }
}
