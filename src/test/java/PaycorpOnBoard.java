import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;


public class PaycorpOnBoard {
  protected WebDriver driver ;
  protected WebDriverUtil driverUtil;

  private final String CLIENT_NAME="PayDirect Onboard AU";
  private final String CUSTOMER="paydirect";
  private final String CUSTOMER_ID="50000599";

  @Test
  public void createIds() throws Exception {
//    List<String> newClientIds = Arrays.asList("10019868","10019869","10019870","10019871","10019872","10019873","10019874","10019875","10019876","10019877","10019878","10019879","10019880","10019881","10019882","10019883","10019884","10019885","10019886","10019887","10019888","10019889","10019890","10019891","10019892","10019893","10019894","10019895","10019896","10019897","10019898","10019899","10019900","10019901","10019902","10019903","10019904","10019905","10019906","10019907","10019908","10019909","10019910","10019911","10019912","10019913","10019914","10019915","10019916","10019917","10019918","10019919","10019920","10019921","10019922","10019923","10019924","10019925","10019926","10019927","10019928","10019929","10019930","10019931","10019932","10019933","10019934","10019935","10019936","10019937","10019938","10019939","10019940","10019941","10019942","10019943","10019944","10019945","10019946","10019947","10019948","10019949","10019950","10019951","10019952","10019953","10019954","10019955","10019956","10019957","10019958","10019959","10019960","10019961","10019962","10019963","10019964","10019965","10019966","10019967","10019968","10019969","10019970","10019971","10019972","10019973","10019974","10019975","10019976","10019977","10019978","10019979","10019980","10019981","10019982","10019983","10019984","10019985","10019986","10019987","10019988","10019989","10019990","10019991","10019992","10019993","10019994","10019995","10019996","10019997","10019998","10019999","10020000","10020001","10020002","10020003","10020004","10020005","10020006","10020007","10020008","10020009","10020010","10020011","10020012","10020013","10020014","10020015","10020016","10020017","10020018","10020019","10020020","10020021","10020022","10020023","10020024","10020025","10020026","10020027","10020028","10020029","10020030","10020031","10020032","10020033","10020034","10020035","10020036","10020037","10020038","10020039","10020040","10020041","10020042","10020043","10020044","10020045","10020046","10020047","10020048","10020049","10020050","10020051","10020052","10020053","10020054","10020055","10020056","10020057","10020058","10020059","10020060","10020061","10020062","10020063","10020064","10020065","10020066","10020067","10020068","10020069","10020070","10020071","10020072","10020073","10020074","10020075","10020076","10020077","10020078","10020079","10020080","10020081","10020082","10020083","10020084","10020085","10020086","10020087","10020088","10020089","10020090","10020091","10020092","10020093","10020094","10020095","10020096","10020097","10020098","10020099","10020100","10020101","10020102","10020103","10020104","10020105","10020106","10020107","10020108","10020109","10020110","10020111","10020112","10020113","10020114","10020115","10020116","10020117","10020118","10020119","10020120","10020121","10020122","10020123","10020124","10020125","10020126","10020127","10020128","10020129","10020130","10020131","10020132","10020133","10020134","10020135","10020136","10020137","10020138","10020139","10020140","10020141","10020142","10020143","10020144","10020145","10020146","10020147","10020148","10020149","10020150","10020151","10020152","10020153","10020154","10020155","10020156","10020157","10020158","10020159","10020160","10020161","10020162","10020163","10020164","10020165","10020166","10020167","10020168","10020169","10020170","10020171","10020172","10020173","10020174","10020175","10020176","10020177","10020178","10020179","10020180","10020181","10020182","10020183","10020184","10020185","10020186","10020187","10020188","10020189","10020190","10020191","10020192","10020193","10020194","10020195","10020196","10020197","10020198","10020199","10020200","10020201","10020202","10020203","10020204","10020205","10020206","10020207","10020208","10020209","10020210","10020211","10020212","10020213","10020214","10020215","10020216","10020217","10020218","10020219","10020220","10020221","10020222","10020223","10020224","10020225","10020226","10020227","10020228","10020229","10020230","10020231","10020232","10020233","10020234","10020235","10020236","10020237","10020238","10020239","10020240","10020241","10020242","10020243","10020244","10020245","10020246","10020247","10020248","10020249","10020250","10020251","10020252","10020253","10020254","10020255","10020256","10020257","10020258","10020259","10020260","10020261","10020262","10020263","10020264","10020265","10020266","10020267","10020268","10020269","10020270","10020271","10020272","10020273","10020274","10020275","10020276","10020277","10020278","10020279","10020280","10020281","10020282","10020283","10020284","10020285","10020286","10020287","10020288","10020289","10020290","10020291","10020292","10020293","10020294","10020295","10020296","10020297","10020298","10020299","10020300","10020301","10020302","10020303","10020304","10020305","10020306","10020307","10020308","10020309","10020310","10020311","10020312","10020313","10020314","10020315","10020316","10020317","10020318","10020319","10020320","10020321","10020322","10020323","10020324","10020325","10020326","10020327","10020328","10020329","10020330","10020331","10020332","10020333","10020334","10020335","10020336","10020337","10020338","10020339","10020340","10020341","10020342","10020343","10020344","10020345","10020346","10020347","10020348","10020349","10020350","10020351","10020352","10020353","10020354","10020355","10020356","10020357","10020358","10020359","10020360","10020361","10020362","10020363","10020364","10020365","10020366","10020367","10020368","10020369","10020370","10020371","10020372","10020373","10020374","10020375","10020376","10020377","10020378","10020379","10020380","10020381","10020382","10020383","10020384","10020385","10020386","10020387","10020388","10020389","10020390","10020391","10020392","10020393","10020394","10020395","10020396","10020397","10020398","10020399","10020400","10020401","10020402","10020403","10020404","10020405","10020406","10020407","10020408","10020409","10020410","10020411","10020412","10020413","10020414","10020415","10020416","10020417","10020418","10020419","10020420","10020421","10020422","10020423","10020424","10020425","10020426","10020427","10020428","10020429","10020430","10020431","10020432","10020433","10020434","10020435","10020436","10020437","10020438","10020439","10020440","10020441","10020442","10020443","10020444","10020445","10020446","10020447","10020448","10020449","10020450","10020451","10020452","10020453","10020454","10020455","10020456","10020457","10020458","10020459","10020460","10020461","10020462","10020463","10020464","10020465","10020466","10020467","10020468","10020469","10020470","10020471","10020472","10020473","10020474","10020475","10020476","10020477","10020478","10020479","10020480","10020481","10020482","10020483","10020484","10020485","10020486","10020487","10020488","10020489","10020490","10020491","10020492","10020493","10020494","10020495","10020496","10020497","10020498","10020499","10020500","10020501","10020502","10020503","10020504","10020505","10020506","10020507","10020508","10020509","10020510","10020511","10020512","10020513","10020514","10020515","10020516","10020517","10020518","10020519","10020520","10020521","10020522","10020523","10020524","10020525","10020526","10020527","10020528","10020529","10020530","10020531","10020532","10020533","10020534","10020535","10020536","10020537","10020538","10020539","10020540","10020541","10020542","10020543","10020544","10020545","10020546","10020547","10020548","10020549","10020550","10020551","10020552","10020553","10020554","10020555","10020556","10020557","10020558","10020559","10020560","10020561","10020562","10020563","10020564","10020565","10020566","10020567","10020568","10020569","10020570","10020571","10020572","10020573","10020574","10020575","10020576","10020577","10020578","10020579","10020580","10020581","10020582","10020583","10020584","10020585","10020586","10020587","10020588","10020589","10020590","10020591","10020592","10020593","10020594","10020595","10020596","10020597","10020598","10020599","10020600","10020601","10020602","10020603","10020604","10020605","10020606","10020607","10020608","10020609","10020610","10020611","10020612","10020613","10020614","10020615","10020616","10020617","10020618","10020619","10020620","10020621","10020622","10020623","10020624","10020625","10020626","10020627","10020628","10020629","10020630","10020631","10020632","10020633","10020634","10020635","10020636","10020637","10020638","10020639","10020640","10020641","10020642","10020643","10020644","10020645","10020646","10020647","10020648","10020649","10020650","10020651","10020652","10020653","10020654","10020655","10020656","10020657","10020658","10020659","10020660","10020661","10020662","10020663","10020664","10020665","10020666","10020667","10020668","10020669","10020670","10020671","10020672","10020673","10020674","10020675");

    int count=300;
    init();
    loginWTSConsole();
    switchToDB(false);
    toCustomerPage("Paydirect");

    System.out.println(new Date().toString() + "---- 1. create client in old wts console ------");
    // create new clients
    for(int i=0; i<count; i++) {
      System.out.println("creating client no:" + i);
      doItOr(j -> createNewClient(), j -> {
        init();
        loginWTSConsole();
        switchToDB(false);
        toCustomerPage("Paydirect");
      }).accept(i);
    }

    System.out.println(new Date().toString() + "---- 2. migrate from test to live db ------");
    List<String> newClientIds = migrateToLive("Paydirect");

    System.out.println(new Date().toString() + "---- 3. configure wts client in live db ------");

    switchToDB(true);
    toCustomerPage("Paydirect");
    driverUtil.switchToFrame("content");

    for (String newClientId : newClientIds) {
      doItOr(j -> configureWtsClient(newClientId), j -> {
        init();
        loginWTSConsole();
        switchToDB(true);
        toCustomerPage("Paydirect");
        driverUtil.switchToFrame("content");
      }).accept(newClientId);
    }

    logOutWtsConsole();

    System.out.println(new Date().toString() + "---- 4. add to new console ------");


    loginNewConsole();

    doItOr(j -> addNewClientToNewConsole(), j -> {
      init();
      loginNewConsole();
    }).accept("");


    System.out.println(new Date().toString() + "---- 5. configure pcw ------");

    newClientIds.forEach(id -> doItOr(j -> configurePCW(id), j -> {
      init();
      loginNewConsole();
    }).accept(id));

    System.out.println(new Date().toString() + ":---- validating ------");
    validate(newClientIds);
    System.out.println(new Date().toString() + ":---- 6. Complete ------");
  }


  @FunctionalInterface
  public interface ThrowingConsumer<T, E extends Exception> {
    void accept(T t) throws E;
  }

  private static <T> Consumer<T> doItOr(
      ThrowingConsumer<T, Exception> doIt, ThrowingConsumer<T, Exception> or) {

    return i -> {
      try {
        doIt.accept(i);
      } catch (Exception ex) {
        try {
          System.out.println("trying to fix");
          or.accept(i);
          doItOr(j->doIt.accept(i), j-> or.accept(i)).accept(i);
        } catch (Exception e) {
          throw new RuntimeException(ex);
        }
      }
    };
  }

  //  @Before
  public void init() throws Exception {
    System.setProperty("webdriver.chrome.driver","/Users/ryan.song/Downloads/chromedriver");

    if(driver != null) {
      try {
        driver.quit();
      } catch (Exception e) {
        // ignore
      }
    }
    driver = new ChromeDriver();

    driverUtil = new WebDriverUtil(driver, 50);

  }

  private void validate(List<String> ids) throws Exception {
    init();
    loginNewConsole();

    for (int i = 0; i < ids.size(); i++) {
      String clientId = ids.get(i);
      try {
        // verify client exists
        driverUtil.getWait(20).until(d -> {
          d.navigate().to("https://console.paycorp.com.au/#/client/" + clientId + "/details");
          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          return d.getCurrentUrl().contains(clientId)
              && driverUtil.getText(By.name("client_id")).equalsIgnoreCase(clientId);
        });

        if (
            driverUtil.getElement(By.name("purchase")).isSelected() &&
                driverUtil.getElement(By.name("refund")).isSelected() &&
                !driverUtil.getElement(By.name("preauth_completion")).isSelected() &&
                driverUtil.getElement(By.id("id_mastercard")).isSelected() &&
                driverUtil.getElement(By.id("id_visa")).isSelected() &&
                !driverUtil.getElement(By.id(("id_amex"))).isSelected() &&
                !driverUtil.getElement(By.id(("id_diners"))).isSelected()) {

          driverUtil.click(By.partialLinkText("Payment Pages"));

          try {
            driverUtil.getWait(15).until(d -> driverUtil.getElement(By.name("3ds")).isSelected());
          } catch (Exception e) {
            System.out.println("3ds not enabled:" + clientId);
            configure3DS(clientId);
            System.out.println("fixed:" + clientId);
            i--;
            continue;
          }

          // verify
          if (driver.findElements(By.linkText("Edit Advanced Payment Page")).size() > 0 &&
              (driverUtil.getText(By.name("merchant_bank")).equalsIgnoreCase("aus_wbc")) &&
              (driverUtil.getText(By.name("merchant_country")).equalsIgnoreCase("Australia")) &&
              (driverUtil.getText(By.name("threshold")).equalsIgnoreCase("0")) &&
              (driverUtil.getText(By.name("merchant_name")).equalsIgnoreCase("PayDirect")) &&
              (driverUtil.getText(By.name("merchant_url")).equalsIgnoreCase("https://merchants.paycorp.com.au")) &&
              (driverUtil.getText(By.xpath("(//input[@title='Id'])[1]")).equalsIgnoreCase("25419490")) &&
              (driverUtil.getText(By.xpath("(//input[@title='Id'])[2]")).equalsIgnoreCase("25419490")) &&
              (driverUtil.getText(By.xpath("(//input[@placeholder='BinId'])[1]")).equalsIgnoreCase("516000")) &&
              (driverUtil.getText(By.xpath("(//input[@placeholder='BinId'])[2]")).equalsIgnoreCase("456471")) &&
              (driverUtil.getText(By.xpath("(//input[@placeholder='Password'])[2]")).equalsIgnoreCase("7Lm6n3pY"))) {
            System.out.print(clientId + ",");

          } else {
            System.out.println("payment error:" + clientId);
            if (driver.findElements(By.linkText("Edit Advanced Payment Page")).size() == 0) {
              configurePaymentPage(clientId);
              i--;
              System.out.println("fixed:" + clientId);
              continue;
            }
          }

        } else {
          System.out.println("detail error:" + clientId);
        }
      } catch (Exception e) {
        System.out.println("error at:" + clientId);
        e.printStackTrace();
      }
    }
  }

  @Test
  public void validate() throws Exception {
    int startClientId=10019266;
    init();

    loginNewConsole();

    for(int i=0; i<100; i++) {
      String clientId = String.valueOf(startClientId + i);
      try {
        // verify client exists
        driverUtil.getWait(20).until(d -> {
          d.navigate().to("https://console.paycorp.com.au/#/client/" + clientId + "/details");
          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          return d.getCurrentUrl().contains(clientId)
              && driverUtil.getText(By.name("client_id")).equalsIgnoreCase(clientId);
        });

        if (
            driverUtil.getElement(By.name("purchase")).isSelected() &&
                driverUtil.getElement(By.name("refund")).isSelected() &&
                !driverUtil.getElement(By.name("preauth_completion")).isSelected() &&
                driverUtil.getElement(By.id("id_mastercard")).isSelected() &&
                driverUtil.getElement(By.id("id_visa")).isSelected() &&
                !driverUtil.getElement(By.id(("id_amex"))).isSelected() &&
                !driverUtil.getElement(By.id(("id_diners"))).isSelected()) {

          driverUtil.click(By.partialLinkText("Payment Pages"));

          try {
            driverUtil.getWait(15).until(d -> driverUtil.getElement(By.name("3ds")).isSelected());
          } catch (Exception e) {
            System.out.println("3ds not enabled:" + clientId);
            configure3DS(clientId);
            System.out.println("fixed:" + clientId);
            i--;
            continue;
          }

          // verify
          if (driver.findElements(By.linkText("Edit Advanced Payment Page")).size() > 0 &&
              (driverUtil.getText(By.name("merchant_bank")).equalsIgnoreCase("aus_wbc")) &&
              (driverUtil.getText(By.name("merchant_country")).equalsIgnoreCase("Australia")) &&
              (driverUtil.getText(By.name("threshold")).equalsIgnoreCase( "0")) &&
              (driverUtil.getText(By.name("merchant_name")).equalsIgnoreCase( "PayDirect")) &&
              (driverUtil.getText(By.name("merchant_url")).equalsIgnoreCase( "https://merchants.paycorp.com.au")) &&
              (driverUtil.getText(By.xpath("(//input[@title='Id'])[1]")).equalsIgnoreCase( "25419490")) &&
              (driverUtil.getText(By.xpath("(//input[@title='Id'])[2]")).equalsIgnoreCase( "25419490")) &&
              (driverUtil.getText(By.xpath("(//input[@placeholder='BinId'])[1]")).equalsIgnoreCase( "516000")) &&
              (driverUtil.getText(By.xpath("(//input[@placeholder='BinId'])[2]")).equalsIgnoreCase( "456471")) &&
              (driverUtil.getText(By.xpath("(//input[@placeholder='Password'])[2]")).equalsIgnoreCase( "7Lm6n3pY"))) {
            System.out.print(clientId + ",");

          } else {
            System.out.println("payment error:" + clientId);
            if (driver.findElements(By.linkText("Edit Advanced Payment Page")).size() == 0) {
              configurePaymentPage(clientId);
              i--;
              System.out.println("fixed:" + clientId);
              continue;
            }
          }

        } else {
          System.out.println("detail error:" + clientId);
        }
      } catch (Exception e) {
        System.out.println("error at:" + clientId);
        e.printStackTrace();
      }
    }
  }

  private void loginWTSConsole() throws Exception {
    driver.get("https://merchants.paycorp.com.au/generic/admin/");
    driver.switchTo().activeElement();
    driver.manage().window().maximize();


    driverUtil.switchToFrame("main");
    driverUtil.setText(By.name("username"), "rsong");
    driverUtil.setText(By.name("password"), "Welcome@1234");
    driverUtil.click(By.cssSelector("input[type='submit']"));
  }

  private void loginNewConsole() throws Exception {
    driver.get("https://console.paycorp.com.au/");
    driver.switchTo().activeElement();
    driver.manage().window().maximize();

    driverUtil.setText(By.name("username"), "ryan.song@myob.com");
    driverUtil.setText(By.name("password"), "cEgdxh4U!!");
    driverUtil.click(By.cssSelector("input[type='submit']"));
  }

  private void logOutWtsConsole() throws Exception {
    driverUtil.switchToFrame("header");
    driverUtil.click(By.linkText("Logout"));
  }

  private void switchToDB(boolean isLive) throws Exception {
    driverUtil.switchToFrame("nav");

    if (isLive && ! driverUtil.getText(By.tagName("body")).contains("Currently viewing live database")) {
      driverUtil.click(By.partialLinkText("[ Change Database ]"));
      driver.switchTo().alert().accept();

      return;
    }

    if (!isLive && driverUtil.getText(By.tagName("body")).contains("Currently viewing live database")) {
      driverUtil.click(By.partialLinkText("[ Change Database ]"));
      driver.switchTo().alert().accept();
    }
  }

  private void createNewClient() throws Exception {
    driverUtil.switchToFrame("content");
    driverUtil.click(By.name("client"));
    driverUtil.switchToWindowByUrl("NewClient");
    driverUtil.setText(By.name("comment"), CLIENT_NAME);
    driverUtil.click(By.name("submit"));
    Thread.sleep(500);
    driverUtil.switchToWindowByUrl("merchant");
  }
  private void configureWtsClient(String clientId) throws Exception {

    System.out.println("start configuring Wts client:" + clientId);

    driverUtil.click(By.linkText(clientId));

    driverUtil.clickUtil(By.linkText("authentication"), By.name("cart_inst"));
    driverUtil.setValue(By.name("cart_inst"), "cartridge:unknown");
    driverUtil.click(By.cssSelector("input[value='save']"));

    driverUtil.clickUtil(By.linkText("cards accepted"),By.name("bankcard"));
    driverUtil.uncheck(By.name("bankcard"));
    driverUtil.uncheck(By.name("amex"));
    driverUtil.uncheck(By.name("JCB"));
    driverUtil.uncheck(By.name("diners"));
    driverUtil.click(By.cssSelector("input[value='save']"));


    driverUtil.clickUtil(By.linkText("txns accepted"),By.name("status"));
    driverUtil.uncheck(By.name("status"));
    driverUtil.check(By.name("debit"));
    driverUtil.click(By.cssSelector("input[value='save']"));

    driverUtil.click(By.linkText("Return to Customer Record"));
  }

  private List<String> migrateToLive(String customerName) throws Exception {
    driverUtil.switchToFrame("nav");
    driverUtil.click(By.linkText("Accounts"));
    driverUtil.click(By.linkText("Migrate Accounts"));

    driverUtil.switchToFrame("content");


    WebElement selectAll = driverUtil.getElement(By.xpath("//table/tbody//b[contains(text(),'" + customerName + "')]/../../td/a"));

    List<String> migrateClients = driver.findElements(By.cssSelector("input[name*='" + CUSTOMER_ID + "']"))
        .stream()
        .map(e -> e.getAttribute("name").replace(CUSTOMER_ID + "^", "").trim())
        .collect(Collectors.toList());

   // assert(migrateClients.size() == count);
    migrateClients.forEach(s -> System.out.print("\"" +s + "\","));

    selectAll.click();

    driverUtil.click(By.cssSelector("input[value*='migrate']"));
    driverUtil.acceptAlert();

    return migrateClients;

  }

  private void toMerchantDetails() throws Exception {
    driverUtil.switchToWindowByUrl("merchant");
    driverUtil.switchToFrame("nav");
    driverUtil.click(By.partialLinkText("Merchant Administration"));
    driverUtil.click(By.partialLinkText("Merchant Details"));
  }

  private void toCustomerPage(String customer) throws Exception {
    toMerchantDetails();

    driverUtil.switchToFrame("content");
    driverUtil.setText(By.name("companyname"), CUSTOMER);
    driverUtil.click(By.name("newBtn"));
    driverUtil.click(By.partialLinkText(customer));
  }


  private void addNewClientToNewConsole() throws InterruptedException {
    driverUtil.getWait().until(d -> {
      driver.get("https://console.paycorp.com.au/#/customer/" + CUSTOMER_ID + "/clients");
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      return driver.findElements(By.linkText("Add Client")).size() > 0;
    });

      int count=0;
      String result = "hasMore";
      while(result.equalsIgnoreCase("hasMore")) {
          System.out.println("adding client no:" + ++count);
          driverUtil.click(By.linkText("Add Client"));
          driverUtil.click(By.name("existingClient"));
         result = driverUtil.getWait(120).until(
            d -> {
              List<WebElement> options = new Select(driverUtil.getElement(By.name("client_id"))).getOptions();
              if (options.size() <= 1) return null;
              // assuming AU is the last item
              if (options.get(options.size() - 1).getText().contains(" AU")) {
                driverUtil.setText(By.name("client_id"), CLIENT_NAME);
                driverUtil.click(By.cssSelector("button[type='submit']"));

                return "hasMore";
              } else {
                return "noMore";
              }
            });
        Thread.sleep(500);
      }
  }

  private void configure3DS(String clientId) {
    System.out.println("enable 3ds for clientId:" + clientId);

      driverUtil.getWait().until(d -> {
        try {
          d.navigate().to("https://console.paycorp.com.au/#/client/" + clientId + "/pcw");

          if (d.getCurrentUrl().contains(clientId)
              && d.findElements(By.name("3ds")).size() > 0) return true;

          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
          return d.getCurrentUrl().contains(clientId)
              && d.findElements(By.name("3ds")).size() > 0;
        } catch (Exception e) {
          return false;
        }
      });


      // enable 3ds
      driverUtil.checkUntil(By.name("3ds"),By.name("merchant_bank"));
      driverUtil.setText(By.name("merchant_bank"), "aus_wbc");
      driverUtil.setText(By.name("merchant_country"), "Australia");
      driverUtil.setText(By.name("threshold"), "0");
      driverUtil.setText(By.name("merchant_name"), "PayDirect");
      driverUtil.setText(By.name("merchant_url"), "https://merchants.paycorp.com.au");
      driverUtil.setText(By.xpath("(//input[@title='Id'])[1]"), "25419490");
      driverUtil.setText(By.xpath("(//input[@title='Id'])[2]"), "25419490");
      driverUtil.setText(By.xpath("(//input[@placeholder='BinId'])[1]"), "516000");
      driverUtil.setText(By.xpath("(//input[@placeholder='BinId'])[2]"), "456471");
      driverUtil.setText(By.xpath("(//input[@placeholder='Password'])[2]"), "7Lm6n3pY");

      driverUtil.click(By.linkText("Update"));
      System.out.println("3ds:" + driverUtil.getElement(By.cssSelector("div.alert")).getText());
//        Thread.sleep(500);
  }

  private void configurePaymentPage(String clientId) {

      System.out.println("configure paymentpage for clientId:" + clientId);

      driverUtil.getWait().until(d -> {
        d.navigate().to("https://console.paycorp.com.au/#/client/" + clientId + "/pcw");

        if (d.getCurrentUrl().contains(clientId)
            && d.findElements(By.linkText("Create Advanced Payment Page")).size() > 0) return true;

        try {
          Thread.sleep(500);
          d.navigate().refresh();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        return d.getCurrentUrl().contains(clientId)
            && d.findElements(By.linkText("Create Advanced Payment Page")).size() > 0;
      });

      driverUtil.click(By.linkText("Create Advanced Payment Page"));
      driverUtil.check(By.name("active"));
      driverUtil.setText(By.name("base_template"), "Basic Iframe Template (v1.0)");

      driver.findElements(By.cssSelector("div.alert button")).forEach(e -> {
        try {
          e.click();
        } catch (Exception e1) {
          //ignore
        }
      });

      driverUtil.clickUtil(By.linkText("Save"), By.name("3ds"), 1);
  }

  private void configurePCW(String clientId) throws Exception {
      configurePaymentPage(clientId);
      configure3DS(clientId);
  }

}
