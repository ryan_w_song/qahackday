import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import sun.nio.ch.IOUtil;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class PdoTest {
  protected WebDriver driver ;
  protected WebDriverUtil driverUtil;

  //@Before
  public void setup() {
    System.setProperty("webdriver.chrome.driver","/Users/ryan.song/Downloads/chromedriver");
    driver = new ChromeDriver();

    driverUtil = new WebDriverUtil(driver, 30);
  }

  @After
  public void cleanup() {
//    driver.close();
//    driver.quit();
  }
  @Test
  public void loginTest() throws Exception {

//    GuiUtil.init();
    GuiUtil gui = new GuiUtil();
//    gui.show(0, 10);
   // gui.show(1, 2);
    gui.show(2, 3);

    setup();
    driver.get("https://fury-au.essentials.dev.myob.com/LA/public.htm");
    driver.switchTo().activeElement();
    driver.manage().window().maximize();

    driverUtil.setText(By.id("UserName"), "fury_pdoautomation@mailinator.com");
    driverUtil.setText(By.id("Password"), "Myob1234");
    driverUtil.click(By.cssSelector("button.btn-primary"));

//    logintoS3();

    int amountInDollor = 10;
    DecimalFormat df2 = new DecimalFormat(".##");
    String invoiceNumber = createInvoice(df2.format(amountInDollor));

    gui.show(3, 3);
    sendEmail(invoiceNumber);

    gui.show(4, 3);
    // go to gmail
    String link = getEmailAndReadyToPay(invoiceNumber);
    driverUtil.switchToNewWindow();
    driver.navigate().to(link);

    gui.show(5, 3);
    payBy("credit card", String.valueOf(amountInDollor*100));
//    payBy("Bpay",  String.valueOf(amountInDollor*100));

    gui.show(6, 3);
    // check invoice status
    driverUtil.switchToWindowByUrl("essentials");
    driver.navigate().refresh();
    verifyInvoiceStatus(invoiceNumber, "Paid");

    gui.show(7, 3);
  }

  private void verifyInvoiceStatus(String invoiceNumber, String status) {
    driverUtil.setText(By.id("search"), invoiceNumber);

    driverUtil.getWait().until(d -> {
      try {
        return d.findElements(By.cssSelector("tbody tr")).stream().filter(r -> {
          List<WebElement> fields = r.findElements(By.tagName("td"));
          if (fields.size() > 1
              && fields.stream().filter(f -> f.getText().equalsIgnoreCase(invoiceNumber)).count() == 1
              && fields.stream().filter(f -> f.getText().equalsIgnoreCase(status)).count() == 1)
            return true;
          return false;
        }).count() == 1;
      } catch (Exception e) {
        return false;
      }
    });
  }

  private String createInvoice(String price) {
    driverUtil.click(By.cssSelector("a[data-automation='sales_group']"));
    driverUtil.click(By.cssSelector("a[data-automation='sales_invoices']"));

    driverUtil.click(By.id("createButton"));

    // fill in forms

    driverUtil.setText(By.id("contactId"), "ABC Service");
    driverUtil.click(By.linkText("ABC Service"));

    driverUtil.setText(By.cssSelector("textarea[name='description']"), "autoItem");
    driverUtil.setText(By.cssSelector("input[name='accountId']"), "Sales 1");
    driverUtil.click(By.linkText("Sales 1"));

//    driverUtil.setText(By.cssSelector("input[name='unitOfMeasure']"), "Qty");
//    driverUtil.setText(By.cssSelector("input[name='quantity']"), "1");
    driverUtil.setText(By.cssSelector("input[name='unitPrice']"), price);
//    driverUtil.setText(By.cssSelector("input[name='taxCode']"), "GST");


    String invoiceId = driverUtil.getText(By.id("invoiceNumber"));

    return invoiceId;

  }


  private void sendEmail(String invoiceId) {
    // send email
    driverUtil.click(By.id("email"));
    driverUtil.click(By.id("emailButton"));

    System.out.println("invoiceId=" + invoiceId);

    driverUtil.click(By.id("save"));

    verifyInvoiceStatus(invoiceId, "Not paid");
  }

  private void getApi() {
    Set<Cookie> allCookies = driver.manage().getCookies();

  }

  private String getEmailAndReadyToPay(String invoiceNumber) throws Exception {
    String url="https://gmail.com";
    ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", url);
    driverUtil.switchToNewWindow();
    driver.navigate().to(url);
    //driverUtil.switchToWindowByUrl("google");
    driverUtil.setText(By.cssSelector("input[type='email']"), "pdo.autotest@gmail.com");
    driverUtil.click(By.id("identifierNext"));

    driverUtil.setText(By.cssSelector("input[type='password']"), "Pdoautotest2018");
    driverUtil.click(By.id("passwordNext"));


    // find email and click it;
    driverUtil.getWait(300).until(d -> {
      // refresh email
      try {
        d.findElements(By.cssSelector("div.UI table tbody tr"))
            .stream()
            .filter(e -> {
              System.out.println("item:" + e.getText());
              return e.getText().contains(invoiceNumber);
            }).findFirst().get().click();
        return true;
      } catch (Exception e) {
        return null;
      }
    });

   WebElement paynow = driverUtil.getElement(By.partialLinkText("Pay now"));
   String link = paynow.getAttribute("href");

   paynow.click();

   return link;
  }


  private void payBy(String type, String amountInCents) throws FileNotFoundException {
    if ("credit card".equalsIgnoreCase(type)) {
      driverUtil.click(By.id("payment-credit-card"));
      payCreditCard();
    } else {
      driverUtil.click(By.id("payment-bpay"));
      String reference = driverUtil.getText(By.cssSelector("span[analytics-label='ref text']"));
      payBpay(amountInCents, reference);
    }
  }

  public void payCreditCard() {
    driverUtil.getElement(By.id("credit-card-payment-frame"));
    driver.switchTo().frame("credit-card-payment-frame");

    driverUtil.setText(By.id("cardHolderName"), "pdoTest");
    driverUtil.setText(By.id("cardNo"), "4564456445644564");
    driverUtil.setText(By.id("cardSecureId"), "123");
    driverUtil.setText(By.id("cardExpiry"), "10/20");
    driverUtil.click(By.cssSelector("button[type='submit'].btn-primary"));


    // verify it's successfully
    driverUtil.getElement(new By.ByCssSelector("span.payment-success-msg"));

  }

  private void logintoS3() throws Exception {
    String url="https://ryan.song@myob.com:Welcome@myob.com1@adfs.myob.com.au/adfs/ls/IdpInitiatedSignOn.aspx";
    ((JavascriptExecutor) driver).executeScript("window.open(arguments[0])", "");
    driverUtil.switchToNewWindow();
    driver.navigate().to(url);

    driverUtil.click(By.id("idp_SignInButton"));

  }
  private void payBpay(String amountInCents, String referenceCode) throws FileNotFoundException {
    String fileContent = "01,CBABPAY,MYOBDSRV,$Today,0930,1,,,2/\n"
        + "02,848283,CBA,1,$Yesterday,,,3/\n"
        + "03,310710102355,,231,1000,2,,250,,0,,550,0,0,/\n"
        + "30,399,$AmountInCents,0,$ReferenceCode,$Comments,0,05,101,$Yesterday,111601,004,,,,,,,,,/\n"
        + "49,2000,3/\n"
        + "98,2000,1,5/\n"
        + "99,2000,1,7/";

    SimpleDateFormat format = new SimpleDateFormat("YYYYMMDD");
    Calendar cal = Calendar.getInstance();
    String today = new SimpleDateFormat("YYYYMMDDmmssSSS").format(cal.getTime());
    fileContent = fileContent.replaceAll("\\$Today", today.substring(0, 8));
    fileContent = fileContent.replaceAll("\\$Comments", "ABCD" + today);
    cal.add(Calendar.DATE, -1);
    fileContent = fileContent.replaceAll("\\$Yesterday", format.format(cal.getTime()));

    fileContent = fileContent.replaceAll("\\$AmountInCents", amountInCents);
    fileContent = fileContent.replaceAll("\\$ReferenceCode", referenceCode);

    PrintWriter out = null;
      out = new PrintWriter(String.format("BPAY-AUTO-%s.pgp", today));
      out.print(fileContent);
    out.close();

    // upload to S3
  }

}
